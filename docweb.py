import urllib.request


class Robot:

    def __init__(self, url):
        self.downloaded = False
        self.url = url

    def retrieve(self):
        while not self.downloaded:
            print(f"Descargando {url1.url}")
            f = urllib.request.urlopen(self.url)
            self.web = (f.read().decode('utf-8'))
            self.downloaded = True

    def show(self):
        print(self.content())

    def content(self):
        self.retrieve()
        return self.web


class Cache:
    def __init__(self):
        self.cache = {}

    def retrieve(self, url):
        while url not in self.cache:
            download = Robot(url = url)
            self.cache[url] = download

    def show(self, url):
        self.retrieve(url)
        print(self.content(url))

    def content(self, url):
        self.retrieve(url)
        return self.cache[url]


url1 = Robot("http://www.python.org/")
url1.retrieve()
url1.show()


url2 = Cache()
url2.retrieve("http://www.python.org/")
url2.show("https://www.urjc.es/")